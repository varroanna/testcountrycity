$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket
	*/
	$.get('/rest/countries', function(response){
		$.each(response, function(index, value){
			var $option = $('<option></option>').text(value.iso);
			$('#city-country').append($option);
		})
	});

	$('body').on('click', '#add-city-button', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut...
		*/
		var cityName=$('#city-name').val();
		var cityPopulation = $('#city-population').val();
		var cityCountry = $('#city-country').val();
		var cityData = {
			"name" : cityName,
			"population" : cityPopulation,
			"country.iso" : cityCountry

		};
		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
			type: "application/x-www-form-urlencoded",
			data : cityData,
			dataType : 'json',
			complete : function(){
				$('.close-modal').trigger('click');
			}
		});
	});

	/*
		TODO: ország törlés gombok működjenek
	*/
	$('table').on('click', '.delete-country', function(){
		var iso = $(this).closest('tr').data('iso');
		if(confirm("Are you sure yo want to delete?")){
			$.ajax({
				url : '/rest/countries/'+iso,
				type : 'DELETE',
				success : (response) =>{
					alert(response);
				}

			});
			$(this).closest('tr').remove();
		}
	});

	/*
		TODO: város törlés gombok működjenek
	*/
	$('table').on('click', '.delete-city', function(){
		var id = $(this).closest('tr').data('id');
		if(confirm('Are you sure you want to delete?')){
			$.ajax({
				url : '/rest/cities/'+id,
				type : 'DELETE',
				success : (response) =>{

				}
			});
			$(this).closest('tr').remove();
		}
	});


});